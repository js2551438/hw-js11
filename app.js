const passwordInput = document.querySelector('#password');
const confirmPasswordInput = document.querySelector('#confirm-password');
const passwordIcons = Array.from(document.querySelectorAll('.input-wrapper-password .icon-password'));
const passwordConfirmIcons = Array.from(document.querySelectorAll('.input-wrapper-confirm-password .icon-password'));
const passwordForm = document.querySelector('.password-form');
const errorMessage = document.querySelector('.error-message');


if (passwordIcons && passwordInput) {
    passwordIcons.forEach((item) => {
        item.addEventListener('click', (el) => {
            if(passwordInput.getAttribute('type') === 'text')  {
                passwordInput.setAttribute('type', 'password');
            }else{
                passwordInput.setAttribute('type', 'text');
            }
            passwordIcons.forEach((item) => {
                item.classList.toggle('d-none');
            })
    })
    })
}

if (confirmPasswordInput && passwordConfirmIcons) {
    passwordConfirmIcons.forEach((item) => {
        item.addEventListener('click' , (el) => {
            if(confirmPasswordInput.getAttribute('type') === 'text') {
                confirmPasswordInput.setAttribute('type', 'password');
            }else {
                confirmPasswordInput.setAttribute('type', 'text');
            }
            passwordConfirmIcons.forEach((item) => {
                item.classList.toggle('d-none');
            })
        })
    })
}

if(passwordForm) {
    passwordForm.addEventListener('submit', (e) => {
        e.preventDefault();
        if(confirmPasswordInput.value === passwordInput.value) {
            errorMessage.classList.add('d-none');
            alert('You are welcome');  
        } else {
            errorMessage.classList.remove('d-none');
        }
    })  
}

